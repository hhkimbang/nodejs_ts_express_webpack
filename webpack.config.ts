import webpack from "webpack";
import path from "path";
import nodeExternals from "webpack-node-externals";
const dotenv = require('dotenv').config( {
    path: path.join(__dirname, '.env')
  } );


module.exports = {
    entry: ["webpack/hot/poll?100", "./src/endtrypoint.ts"],
    watch: true,
    target: "node",
    externals: [
        nodeExternals({
            allowlist: ["webpack/hot/poll?100"]
        })
    ],
    module: {
        rules: [
            {
                test: /.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/
            }
        ]
    },
    mode: "development",
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    plugins: [new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
        "process.env": dotenv.parsed
    }),],
    output: {
        path: path.join(__dirname, "dist"),
        filename: "endtrypoint.js"
    }
};
