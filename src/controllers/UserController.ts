import { Controller, Delete, Get, Post, Put } from '@overnightjs/core';
import { Request, Response } from 'express';
import usrS from '../services/UserService';
import { Usr } from "../entity/User";
import { Profile } from "../entity/Profile";

@Controller('api/user/')
export class UserController {

    UserService = new usrS();
    // api 1 - create usr
    @Post()
    private async createUser(req: Request, res: Response) {
        let usr = new Usr(req.body.usr.usr, req.body.usr.pwd, req.body.usr.fullname);
        let profile = new Profile(req.body.profile.id,
            req.body.profile.birthday, req.body.profile.address, req.body.profile.phone);
        this.UserService.createUser(usr, profile);
        res.status(200).json('ok');
    }

    // api 2 - update usr
    @Put(':id')
    private async updateUser(req: Request, res: Response) {
        let usr = new Usr(req.body.usr.usr, req.body.usr.pwd, req.body.usr.fullname);
        let profile = new Profile(req.body.profile.id,
            req.body.profile.birthday, req.body.profile.address, req.body.profile.phone);
        this.UserService.updateUser(usr, profile,+req.params.id);
        res.status(200).json('ok');
    }

    // // api 3 - get user by id
    // @Get()
    // private async getUsrList(req: Request, res: Response) {
    //     let retVal = await this.UserService.getAllUser();
    //     return res.status(200).json(retVal);
    // }

    // // api 4 - get user by id
    // @Get(':id')
    // private async get(req: Request, res: Response) {
    //     let retVal = await this.UserService.getUserById(+req.params.id);
    //     return res.status(200).json(retVal);
    // }

    // api 5 - delete user by id
    @Delete(':id')
    private async deleteUser(req: Request, res: Response) {
        await this.UserService.deleteUserById(+req.params.id);
        return res.status(200).json('ok');
    }
}