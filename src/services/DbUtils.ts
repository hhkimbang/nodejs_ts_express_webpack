import { createConnection, getConnection, Connection } from "typeorm";
import { Usr } from "../entity/User";
import { Profile } from "../entity/Profile";

export class DbUtils {

    async connectDB() {
        await createConnection({
            type: "mysql",
            host: "localhost",
            port: 3306,
            username: "root",
            password: "123456",
            database: "test",
            entities: [
                Usr,
                Profile
            ],
            synchronize: true,
            logging: false
        }).then(connection => {
            console.log("connected")
        }).catch(error => {
            console.log(error);
        });

        return getConnection();
    }

    closeConnection(connection: Connection) {
        connection.close();
    }
}
