import * as DbUtils from './DbUtils';
import { Usr } from "../entity/User";
import { Profile } from "../entity/Profile";
import { Connection } from "typeorm";

class UserService {

    db = new DbUtils.DbUtils();

    async createUser(usr: Usr, profile: Profile) {
        let connection: Connection = await this.db.connectDB();
        await connection.manager.save(usr).then(async row => {
            profile.pid = row.sid;
            await connection.manager.save(profile);
        }
        ).catch(err => {
            this.db.closeConnection(connection);
            console.log(err);
        });
        this.db.closeConnection(connection);
    }

    async updateUser(usr: Usr, profile: Profile, id: number) {
        let connection: Connection = await this.db.connectDB();
        usr.sid = id;
        await connection.manager.save(usr).then(async row => {
            profile.pid = row.sid;
            await connection.manager.save(profile);
        }
        ).catch(err => {
            this.db.closeConnection(connection);
            console.log(err);
        });
        this.db.closeConnection(connection);
    }

    // async getUserById(id: number) {
    //     let db = new DbUtils.DbUtils();
    //     let rs: any = 0;
    //     await db.query('SELECT * FROM usr WHERE id=?', id).then(rows => {
    //         rs = rows;
    //         db.closeConnection();
    //     }).catch(err => console.log(err));
    //     return rs;
    // }

    // async getAllUser() {
    //     let db = new DbUtils.DbUtils();
    //     let rs: any = 0;
    //     await db.query('SELECT * FROM usr', '').then(rows => {
    //         rs = rows;
    //         db.closeConnection()
    //     }).catch(err => console.log(err));
    //     return rs;
    // }

    async deleteUserById(id: number) {
        let connection: Connection = await this.db.connectDB();
        await connection.manager.delete(Profile, id).then(async () => {
            await connection.manager.delete(Usr, id);
        }).catch(err => {
            this.db.closeConnection(connection);
            console.log(err);
        });
    }
}

export default UserService