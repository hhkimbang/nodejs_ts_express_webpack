import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
class Usr {

    @PrimaryGeneratedColumn({
        name:"id",
        type:"int"
    })
    sid: number;

    @Column()
    usr: string;

    @Column()
    pwd: string;

    @Column()
    fullname: string;

    constructor(usr: string, pwd: string, fullname: string) {
        this.usr = usr;
        this.pwd = pwd;
        this.fullname = fullname;
    }
}

export { Usr }