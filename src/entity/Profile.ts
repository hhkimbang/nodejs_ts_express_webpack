import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
class Profile {

    @PrimaryColumn({
        name:"id",
        default: 1
    })
    pid: number;

    @Column()
    birthday: Date;

    @Column()
    address: string;

    @Column()
    phone: string;

    constructor(id: number, birthday: Date, address: string, phone: string) {
        this.pid = id;
        this.birthday = birthday;
        this.address = address;
        this.phone = phone;
    }
}

export { Profile }